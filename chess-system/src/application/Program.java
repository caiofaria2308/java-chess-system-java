/**
 * 
 */
package application;

import boardgame.Board;
import chess.ChessMatch;

/**
 * @author cnogueira
 *
 */
public class Program {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ChessMatch chessMatch = new ChessMatch();
		UI.printBoard(chessMatch.getPieces());

	}

}
