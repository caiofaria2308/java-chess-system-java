/**
 * 
 */
package boardgame;

/**
 * @author cnogueira
 *
 */
public class Piece {
	protected Position position;
	private Board board;
	/**
	 * @param board
	 */
	public Piece(Board board) {
		this.board = board;
	}
	/**
	 * @return the board
	 */
	protected Board getBoard() {
		return board;
	}
	
	
}
