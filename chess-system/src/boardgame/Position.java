/**
 * 
 */
package boardgame;

/**
 * @author cnogueira
 *
 */
public class Position {
	private Integer row;
	private Integer column;
	/**
	 * @param row
	 * @param column
	 */
	public Position(Integer row, Integer column) {
		this.row = row;
		this.column = column;
	}
	/**
	 * @return the row
	 */
	public Integer getRow() {
		return row;
	}
	/**
	 * @return the column
	 */
	public Integer getColumn() {
		return column;
	}
	/**
	 * @param row the row to set
	 */
	public void setRow(Integer row) {
		this.row = row;
	}
	/**
	 * @param column the column to set
	 */
	public void setColumn(Integer column) {
		this.column = column;
	}
	
	@Override
	public String toString() {
		return this.row + ", " + this.column;
	}
	
	
}
