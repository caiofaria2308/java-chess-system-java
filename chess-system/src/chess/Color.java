/**
 * 
 */
package chess;

/**
 * @author cnogueira
 *
 */
public enum Color {
	BLACK,
	WHITE
}
